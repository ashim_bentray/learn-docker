import { Injectable } from '@nestjs/common';
import { AnyCnameRecord } from 'dns';

@Injectable()
export class AppService {
  getHello(): any {
    return {
      message: 'Hello World!',
    };
  }
}
