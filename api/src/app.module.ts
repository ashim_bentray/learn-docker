import { UserModule } from './models/user/user.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConfigModule } from './config/app/config.module';
import { DatabaseConfigModule } from './config/database/config.module';
import { DatabaseConfigService } from './config/database/config.service';
import { TodoController } from './models/todo/todo.controller';
import { TodoModule } from './models/todo/todo.module';
import { DatabaseProviderModule } from './providers/database/provider.module';

@Module({
  imports: [
    UserModule,
    AppConfigModule,
    DatabaseConfigModule,
    DatabaseProviderModule,
    DatabaseProviderModule,
    TodoModule,
  ],
  controllers: [AppController, TodoController],
  providers: [DatabaseConfigService, AppService],
})
export class AppModule {}
