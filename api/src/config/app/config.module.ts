import { AppConfigService } from './config.service';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
  ],
  exports: [ConfigService, AppConfigService],
  providers: [ConfigService, AppConfigService],
})
export class AppConfigModule {}
