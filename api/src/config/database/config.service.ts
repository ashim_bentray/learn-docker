/*
https://docs.nestjs.com/providers#services
*/

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DatabaseConfigService {
  constructor(private configService: ConfigService) {}

  get type(): string {
    return this.configService.get<string>('db.type');
  }

  get url(): string {
    return this.configService.get<string>('db.url');
  }

  get tblPrefix(): string {
    return this.configService.get<string>('db.tblPrefix');
  }
}
