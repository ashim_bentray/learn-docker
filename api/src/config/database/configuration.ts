import { registerAs } from '@nestjs/config';

export default registerAs('db', () => ({
  type: process.env.DATABASE_TYPE,
  url: process.env.DATABASE,
  tblPrefix: process.env.DATABASE_TABLE_PREFIX,
}));
