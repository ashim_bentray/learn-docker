import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export interface IErrorResponse {
  status: number;
  message: string;
}

export interface IResponse<T> {
  error: null | IErrorResponse;
  data: null | T;
}

@Injectable()
export class ResponseInterceptor<T>
  implements NestInterceptor<T, IResponse<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<IResponse<T>> {
    return next.handle().pipe(
      map((data) => ({
        error: null,
        data,
      })),
      catchError((err) => {
        context.switchToHttp().getResponse().status(err.status);
        return of({
          error: {
            status: err.status,
            message: err.message,
          },
          data: null,
        });
      }),
    );
  }
}
