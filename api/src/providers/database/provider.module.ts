/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { AppConfigModule } from 'src/config/app/config.module';
import { AppConfigService } from 'src/config/app/config.service';
import { DatabaseConfigModule } from 'src/config/database/config.module';
import { DatabaseConfigService } from 'src/config/database/config.service';
import { DatabaseType } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [DatabaseConfigModule, AppConfigModule],
      useFactory: async (db: DatabaseConfigService, app: AppConfigService) => {
        return {
          url: db.url,
          type: db.type as DatabaseType,
          cli: {
            migrationsDir: 'src/db/migration',
          },
          entities: ['dist/**/*.entity{.ts,.js}'],
          migrations: ['dist/db/migration/*{.ts,.js}'],
          subscribers: ['dist/db/subscriber/*{.ts,.js}'],
          synchronize: app.isDevelopment,
          logging: app.isDevelopment,
          entityPrefix: db.tblPrefix
        };
      },
      inject: [DatabaseConfigService, AppConfigService],
    } as TypeOrmModuleAsyncOptions),
  ],
})
export class DatabaseProviderModule {}
