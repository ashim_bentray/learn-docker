import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Delete,
  UseInterceptors,
} from '@nestjs/common';
import { ResponseInterceptor } from 'src/common/interceptors/response.interceptor';
import { CreateTaskDto } from './dto/create-task.dto';
import { TodoService } from './todo.service';

@Controller('todo')
@UseInterceptors(ResponseInterceptor)
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get('/all')
  async all() {
    return await this.todoService.all();
  }

  @Post('/create')
  async create(@Body() input: CreateTaskDto) {
    return await this.todoService.createTask(input);
  }

  @Put('/done/:id')
  async done(@Param('id') id: string) {
    return await this.todoService.markDone(+id);
  }

  @Delete('/:id')
  async delete(@Param('id') id: string) {
    return await this.todoService.remove(+id);
  }
}
