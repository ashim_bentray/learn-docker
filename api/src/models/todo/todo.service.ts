import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { Todo } from './entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
  ) {}

  async all(): Promise<Todo[]> {
    throw new NotFoundException("Didn't find the tasks.");
  }

  async createTask(createTask: CreateTaskDto): Promise<Todo> {
    const todo = this.todoRepository.create(createTask);

    return this.todoRepository.save(todo);
  }

  async markDone(id: number): Promise<Todo> {
    const todo = await this.todoRepository.findOne(id);
    todo.isDone = true;

    return this.todoRepository.save(todo);
  }

  async remove(id: number): Promise<Todo> {
    const todo = await this.todoRepository.findOne(id);

    return this.todoRepository.remove(todo);
  }
}
