export interface ITodo {
  id: number;
  title: string;
  description?: null | string;
  isDone?: boolean;
}
