import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ITodo } from '../interfaces/toto.interface';

@Entity()
export class Todo implements ITodo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({
    type: 'text',
    nullable: true,
    default: null,
  })
  description?: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  isDone?: boolean;
}
